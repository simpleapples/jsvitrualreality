/* main.js */

/*
Copyright (c) 2008 wayne a. lee
Copyright (c) 2012 Zhiya Zang

This demo is modified by Zhiya Zang to support Firefox, Safari, Opera, Chrome, Mobile Safari(iOS5), Android(3.0+). if you have any question, welcome to contract with me.
Mail:zangzhiya@gmail.com

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

var rotY;
var rotX;
var lastX;
var lastY;
var activeTouchId = 0;

function init()
{
	rotY = 0;
	rotX = 0;
	hideUrlBar();
	var e = document.getElementById("controller");
	if (hasTouchEvent()) {
		document.addEventListener('touchstart',function(e){
			activeTouchId = 1;  
			lastX = e.touches[0].pageX;
			lastY = e.touches[0].pageY;
		},false);
		document.addEventListener('touchend',function(e){
			activeTouchId = 0;
		},false);
		document.addEventListener('touchmove',function(e){
			if(activeTouchId > 0) {
				curX = e.changedTouches[0].pageX;
				curY = e.changedTouches[0].pageY;
				rotateByTouch(lastX, lastY, curX, curY);    
				lastX = curX;
				lastY = curY;
			}
		},false); 
	} else {
		window.onmousemove = mouseMove;
		window.onmousedown = mouseDown;
		window.onmouseup = mouseUp;
	}
	if(has3d()) {
		var touch = document.getElementById("display");
		touch.innerHTML = "supported";
	}
	var loadingE = document.getElementById("loading");
	loadingE.parentNode.removeChild(loadingE);
}

function has3d() {
    var el = document.createElement('p'), 
        has3d,
        transforms = {
            'WebkitTransform':'-webkit-transform',
            'OTransform':'-o-transform',
            'MSTransform':'-ms-transform',
            'MozTransform':'-moz-transform',
            'Transform':'transform'
        };

    document.body.insertBefore(el, null);

    for (var t in transforms) {
        if (el.style[t] !== undefined) {
            el.style[t] = "translate3d(1px,1px,1px)";
            has3d = window.getComputedStyle(el).getPropertyValue(transforms[t]);
        }
    }

    document.body.removeChild(el);
    return (has3d !== undefined && has3d.length > 0 && has3d !== "none");
}

function mouseDown(e)
{
	activeTouchId = 1;
    //  take the first touch
    lastX = e.pageX;
    lastY = e.pageY;
    var touch = document.getElementById("display");
    touch.innerHTML = "X:" + lastX + "Y:" + lastY;
}
function mouseMove(e)
{
	if(activeTouchId > 0) {
		rotateByTouch(lastX, lastY, e.clientX, e.clientY);
		lastX = e.clientX;
		lastY = e.clientY;
	}
}

function mouseUp(e) {
	activeTouchId = 0;
}

function rotateByTouch(lastX, lastY, curX, curY)
{
    var e = document.getElementById('cube');
    if ( ! e)
        return;
    rotY -= (curX - lastX) * 0.25;
    rotX += (curY - lastY) * 0.25;
    rotX = Math.max(-88, Math.min(88, rotX));                 
    var touch = document.getElementById("display");
    touch.innerHTML = "X:" + rotX + "Y:" + rotY;
    e.style.webkitTransform = 'translateZ(200px) rotateX(' + rotX + 'deg) rotateY(' + rotY + 'deg)';
    e.style.mozTransform = 'translateZ(200px) rotateX(' + rotX + 'deg) rotateY(' + rotY + 'deg)';
    e.style.oTransform = 'translateZ(200px) rotateX(' + rotX + 'deg) rotateY(' + rotY + 'deg)';
    e.style.transform = 'translateZ(200px) rotateX(' + rotX + 'deg) rotateY(' + rotY + 'deg)';
}

function hasTouchEvent()
{
    if (document && document.createEvent) {
        try {
            document.createEvent('TouchEvent');
            return true;
        }
        catch (e) {
            //  silently fail
        }
    }
    return false;
}


function hideUrlBar()
{
   setTimeout(function () { window.scrollTo(0, 1) }, 100);
}
